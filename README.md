Clone the directory with 

    git clone --recursive

For submission to BAF, you will need to change to the respective branch, and then do (not necessary on lxplus):
    git checkout BAF
    git submodule init
    git submodule update

to run you need a snakemake environment on the submit node, as well as in the job. In Bonn need to run:

    module load anaconda/2022.10-py39
    source activate
    source activate /cephfs/user/s6luvomb/venvs/snakemake
    snakemake -j 1 --profile condor-profile

On lxplus:
    source /eos/user/l/lvomberg/venvs/snakemake/bin/activate
    snakemake -j 1 --profile condor-profile


Note that I cannot guarantee that I will keep this environment alive and unchanged. To set it up yourself, use:

    module load anaconda/2022.10-py39
    conda create -p snakemake -c conda-forge mamba
    source snakemake/bin/activate
    mamba install -c conda-forge -c bioconda snakemake==7.32.4

This will install the environment at your current location. This will install snakemake version 7.32.4, for which I have tested this. Snakemake version 8 changes how cluster submission works in a major way. You will also need to update the location to the env in condor_jobscript.sh.
Unfortunately the env has about 30000 files, so quota could be an issue.  It might be advisable to do this in an interactive job and create a tarball from the env and save that somwhere. I had some issues with this, but it should be doable. Keep in mind that this environment needs to be saved to a location that is available both on the submit node, and from within the job. Ideally it could be included in setupATLAS, one would have to lobby for this. LHCb does have it in their standard software though.
