rule create_txt:
    input:
        "Hello.txt"
    output:
        "data/olleH.txt"
    shell:
        "tac {input} > {output}"